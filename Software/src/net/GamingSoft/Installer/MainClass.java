package net.GamingSoft.Installer;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

public class MainClass extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainClass frame = new MainClass();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainClass() {
		setType(Type.UTILITY);
		setResizable(false);
		setAlwaysOnTop(true);
		setTitle("GamingSoft Installer - GamingSoft Launcher");
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainClass.class.getResource("/net/GamingSoft/Installer/icon.png")));
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 600, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel details = new JLabel("Details:");
		details.setFont(new Font("Roboto Light", Font.PLAIN, 42));
		details.setBounds(10, 11, 564, 59);
		contentPane.add(details);
		
		JTextPane txt = new JTextPane();
		txt.setFont(new Font("Roboto Light", Font.PLAIN, 27));
		txt.setText("Installing Hardrive: C:\\\r\nProduct: GamingSoft Launcher\r\nLicense: June 2019+\r\nFile Size: unknown\r\n");
		txt.setEditable(false);
		txt.setBounds(10, 81, 564, 399);
		contentPane.add(txt);
		
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		cancel.setForeground(Color.WHITE);
		cancel.setFont(new Font("Roboto Light", Font.PLAIN, 24));
		cancel.setBackground(Color.DARK_GRAY);
		cancel.setBounds(388, 491, 186, 59);
		contentPane.add(cancel);
		
		JButton install = new JButton("Install Launcher");
		install.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancel.setEnabled(false);
				install.setEnabled(false);
				txt.setText("Installing...");
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
					System.exit(1);
				}
				
				txt.setText("Finished.");
				cancel.setText("Finish");
				cancel.setEnabled(true);
			}
		});
		
		install.setBackground(Color.DARK_GRAY);
		install.setForeground(Color.WHITE);
		install.setFont(new Font("Roboto Light", Font.PLAIN, 24));
		install.setBounds(10, 491, 368, 59);
		contentPane.add(install);
	}
}
